category = Category.find_or_create_by({name: "Programación"})
category.subcategories.create([
    {name: "Desarrollo Web"},
    {name: "Aplicaciones Móviles"},
    {name: "Lenguajes de Programación"},
    {name: "Desarrollo de Videojuegos"},
    {name: "Bases de Datos"},
    {name: "Testeo de Software"},
    {name: "Ingeniería de Software"},
    {name: "Herramientas de Desarrollo"},
    {name: "Comercio Electrónico"}
])

category = Category.find_or_create_by({name: "IT y Software"})
category.subcategories.create([
    {name: "Certificaciones IT"},
    {name: "Redes y Seguridad"},
    {name: "Hardware"},
    {name: "Sistemas Operativos"},
    {name: "Otro"}
])

category = Category.find_or_create_by({name: "Productividad de Oficina"})
category.subcategories.create([
    {name: "Microsoft"},
    {name: "Apple"},
    {name: "Google"},
    {name: "SAP"},
    {name: "Intuit"},
    {name: "Departamento de Ventas"},
    {name: "Oracle"},
    {name: "Otro"}
])

category = Category.find_or_create_by({name: "Diseño"})
category.subcategories.create([
    {name: "Diseño Web"},
    {name: "Diseño Gráfico"},
    {name: "Herramientas de Diseño"},
    {name: "Experiencia de Usuario"},
    {name: "Diseño de Juegos"},
    {name: "Design Thinking - Innovación en los Negocios"},
    {name: "3D y Animación"},
    {name: "Moda"},
    {name: "Diseño Arquitectónico"},
    {name: "Diseño de Interiores"},
    {name: "Otro"}
])

category = Category.find_or_create_by({name: "Marketing"})
category.subcategories.create([
    {name: "Marketing Digital"},
    {name: "SEO - Optimización de Motores de Búsqueda"},
    {name: "Marketing de Redes Sociales"},
    {name: "Branding"},
    {name: "Fundamentos de Marketing"},
    {name: "Estadísticas y Automatización"},
    {name: "Relaciones Públicas"},
    {name: "Promoción"},
    {name: "Márketing con Vídeo y Móvil"},
    {name: "Márketing de Contenidos"},
    {name: "Marketing Tradicional"},
    {name: "Growth Hacking"},
    {name: "Marketing de Afiliados"},
    {name: "Marketing de Producto"},
    {name: "Otro"}
])