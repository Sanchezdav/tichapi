class CreateSubcategories < ActiveRecord::Migration[5.0]
  def change
    create_table :subcategories do |t|
      t.references :category, foreign_key: true, on_delete: :cascade
      t.string :name
      t.string :slug

      t.timestamps
    end
  end
end
