class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.references :user, foreign_key: true, on_delete: :cascade
      t.string :title
      t.text :content
      t.string :image
      t.references :subcategory, foreign_key: true, on_delete: :cascade
      t.string :slug
      t.integer :count_views, default: 0
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
