class CreateDiscussions < ActiveRecord::Migration[5.0]
  def change
    create_table :discussions do |t|
      t.references :user, foreign_key: true, index: true, on_delete: :cascade
      t.text :message
      t.integer :discussion_id, index: true
      t.references :discussionable, polymorphic: true, index: true
      t.integer :comments_count, default: 0, null: false
      t.foreign_key :discussions

      t.timestamps
    end
  end
end
