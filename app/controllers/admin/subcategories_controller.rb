class Admin::SubcategoriesController < Admin::ApplicationController
  before_action :set_category

  def index
    @subcategories = @category.subcategories.order(name: :asc)
    render json: @subcategories
  end

  private
  def set_category
    @category = Category.find(params[:category_id])
  end
end