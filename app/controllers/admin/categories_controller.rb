class Admin::CategoriesController < Admin::ApplicationController
  def index
    @categories = Category.order(name: :asc)
    render json: @categories
  end
end