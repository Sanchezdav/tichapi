class Admin::ArticlesController < Admin::ApplicationController
  before_action :set_article, only: [:show, :update, :destroy]

  def index
    @articles = Article.recents
    render json: @articles
  end

  def show
    render json: @article
  end

  def create
    @article = current_user.articles.create(article_params)

    if @article.save
      render json: @article
    else
      render :status => :unprocessable_entity,:json => @article.errors
    end
  end

  def update
    if params[:file].blank?
      if params[:title] != @article.title
        @article.slug = nil
        update_article
      else
        update_article
      end
    else
      if @article.update_attributes(:image => params[:file])
        render json: @article
      else
        render :status => :unprocessable_entity, :json => @article.errors
      end
    end

  end
  
  def update_article
    if @article.update_attributes(article_params)
      render json: @article
    else
      render :status => :unprocessable_entity,:json => @article.errors
    end
  end

  def destroy
    if @article.destroy
      msg = { status: 200, :message => "Eliminado correctamente!" }
      render :status => :ok, :json => msg
    else
      render :status => :unprocessable_entity,:json => @article.errors
    end
  end

  private
  def set_article
    @article = current_user.articles.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:title, :content, :content_markdown, :image, :subcategory_id, :published)
  end
end