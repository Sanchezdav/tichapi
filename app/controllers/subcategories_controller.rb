class SubcategoriesController < ApplicationController
  def index
    @subcategories = Subcategory.all
    @subcategories = @subcategories.joins(:category).where(categories: {slug: params[:category]}) if params[:category].present?
    render json: @subcategories
  end
end
