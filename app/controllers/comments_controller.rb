class CommentsController < ApplicationController
  before_action :set_comment, only: :update
  before_action :set_discussion, only: :create
  before_action :authenticate_user!, except: :index

  def create
    @comment = @discussion.comments.create(comment_params)
    @comment.update_attributes(user_id: current_user.id)

    if @comment.save
      render json: @comment, include: [:user]
    else
      render :status => :unprocessable_entity,:json => @comment.errors
    end
  end

  def update
    if @comment.update(comment_params)
      render json: @comment, include: [:user]
    else
      render :status => :unprocessable_entity,:json => @comment.errors
    end
  end

  private
  def set_comment
    set_discussion
    @comment = @discussion.comments.find(params[:id])
  end

  def set_discussion
    @discussion = Discussion.find(params[:discussion_id])
  end

  def comment_params
    params.require(:comment).permit(:message)
  end
end