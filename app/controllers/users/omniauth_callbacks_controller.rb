class Users::OmniauthCallbacksController < DeviseTokenAuth::OmniauthCallbacksController
  def omniauth_success
    super
    profile=Profile.find_by({
      user_id: @resource.id
    })
    if profile.alias.nil?
      profile.alias = SecureRandom.hex(10)
      profile.save
    end
    profile.update_attributes({
      name: auth_hash['info']["name"]
    }) if profile.name.blank?
    profile.save()
  end

 def assign_provider_attrs(user, auth_hash)
    # if auth_hash['info']['image']
    #   case user.provider
    #   when "facebook"
    #     avatar_url = process_uri(auth_hash['info']['image']+"?width=900&height=900")
    #   when "google_oauth2"
    #     avatar_url = process_uri(auth_hash["info"]["image"].gsub(/\?.+/,"?sz=900"))
    #   when "twitter"
    #     avatar_url = process_uri(auth_hash['info']['image'].gsub(/_normal/,""))
    #   else
    #     avatar_url = process_uri(auth_hash["info"]["image"])
    #   end
    #   user.assign_attributes(remote_avatar_url: avatar_url)
    # end

    if auth_hash['info']['email']
      user.assign_attributes({
        email: auth_hash['info']['email']
      })
    end

  end

    def process_uri(uri)
      require 'open-uri'
      require 'open_uri_redirections'
      open(uri, :allow_redirections => :safe) do |r|
        r.base_uri.to_s
      end
    end

end
