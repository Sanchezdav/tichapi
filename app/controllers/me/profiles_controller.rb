class Me::ProfilesController < Me::ApplicationController
  before_action :set_profile, only: [:show, :update]

  def show
    render json: @profile
  end

  def update
    if params[:file].blank?
      if @profile.update_attributes(profile_params)
        render json: @profile
      else
        render :status => :unprocessable_entity,:json => @profile.errors.full_messages
      end
    else
      @user = current_user
      @user.update_attributes(:avatar => params[:file])
      if @user.update_attributes(:avatar => params[:file])
        render json: @user
      else
        render json: @user.errors.full_messages
      end
    end
  end

  private
  def set_profile
    @profile = current_user.profile
  end

  def profile_params
    params.require(:profile).permit(:name, :last, :bio, :alias, :country)
  end
end