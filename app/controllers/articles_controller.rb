class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :update]

  def index
    @articles = Article.published.recents
    render json: @articles
  end

  def show
    render json: @article
  end

  def update
    if @article.update_attributes(article_params)
      render json: @article
    else
      render :status => :unprocessable_entity,:json => @article.errors
    end
  end

  private
  def set_article
    @article = Article.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:count_views)
  end
end