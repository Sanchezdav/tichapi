class CategoriesController < ApplicationController
  api :GET, 'categories', 'List categories'
  description 'List all categories'
  def index
    @categories = Category.all.order(name: :asc)
    render json: @categories
  end
end
