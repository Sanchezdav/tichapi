class DiscussionsController < ApplicationController
  before_action :set_discussion, only: :update
  before_action :set_article, only: [:index, :create]
  before_action :authenticate_user!, except: :index

  def index
    @discussions = @article.discussions.eager_load(user: :profile).recents
    render json: @discussions, include: [:user, comments: :user]
  end

  def create
    @discussion = @article.discussions.create(discussion_params)
    @discussion.update_attributes(user_id: current_user.id)

    if @discussion.save
      render json: @discussion
    else
      render :status => :unprocessable_entity,:json => @discussion.errors
    end
  end

  def update
    if @discussion.update(discussion_params)
      render json: @discussion
    else
      render :status => :unprocessable_entity,:json => @discussion.errors
    end
  end

  private
  def set_discussion
    @discussion = current_user.discussions.find(params[:id])
  end

  def set_article
    @article = Article.find(params[:article_id])
  end

  def discussion_params
    params.require(:discussion).permit(:message)
  end
end