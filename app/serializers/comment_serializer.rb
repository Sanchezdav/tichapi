class CommentSerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper
  attributes :id, :message, :created_at

  def created_at
    object.created_at = time_ago_in_words(object.created_at)
  end
end