class CategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :slug

  # def subcategories
  #   object.subcategories.map do |subcategory|
  #     SubcategorySerializer.new(subcategory, scope: scope, root: false, subcategory: object)
  #   end
  # end
end
