class ArticleSerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper
  attributes :id, :title, :content, :content_markdown, :image, :slug, :count_views, :subcategory_id, :published, :created_at, :comments_count
  belongs_to :category
  belongs_to :subcategory
  belongs_to :user

  def created_at
    object.created_at = time_ago_in_words(object.created_at)
  end
end
