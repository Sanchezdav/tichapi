class SubcategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :slug
  # has_one :category
end
