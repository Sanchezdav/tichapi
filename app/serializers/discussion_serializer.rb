class DiscussionSerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper
  attributes :id, :message, :comments_count, :discussionable_id, :discussionable_type,:created_at
  belongs_to :user
  has_one :discussionable, :polymorphic => true
  has_many :comments

  def created_at
    object.created_at = time_ago_in_words(object.created_at)
  end
end