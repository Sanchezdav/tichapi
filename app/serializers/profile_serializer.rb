class ProfileSerializer < ActiveModel::Serializer
  attributes :id, :name, :last, :full_name, :bio, :alias, :country
  belongs_to :user

  def full_name
    if !object.name.nil? and object.last.nil?
      object.name
    elsif object.name.nil? and !object.last.nil?
      object.last
    elsif !object.name.nil? and !object.last.nil?
      object.name + " " + object.last
    elsif object.name.nil? and object.last.nil?
      nil
    end
  end
end
