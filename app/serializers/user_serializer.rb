class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :avatar, :profile
  
  def profile
    ProfileSerializer.new(object.profile).attributes
  end
end
