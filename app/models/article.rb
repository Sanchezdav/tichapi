class Article < ApplicationRecord
  belongs_to :user
  belongs_to :subcategory
  has_one :category, through: :subcategory
  has_many :discussions, as: :discussionable
  validates :title, presence: { message: "El título es obligatorio" }, length: { minimum: 5 }
  validates :content, length: { minimum: 10 }, allow_blank: true
  validates :subcategory_id, presence: { message: "La subcategoría es obligatoria" }
  before_create :default_count_views

  mount_uploader :image, ArticleImageUploader
  validates_integrity_of  :image
  validates_processing_of :image

  acts_as_paranoid

  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  scope :published, ->{ where(published: true) }
  scope :recents, ->{ order(created_at: :desc) }

  private
  def default_count_views
    self.count_views ||= 0
  end
end
