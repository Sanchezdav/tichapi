class Subcategory < ApplicationRecord
  belongs_to :category
  has_many :articles, :dependent => :destroy
  validates :name, presence: true, uniqueness: true
  validates :slug, uniqueness: true

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]
end
