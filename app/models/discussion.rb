class Discussion < ApplicationRecord
  belongs_to :discussionable, polymorphic: true, :counter_cache => :comments_count
  belongs_to :user
  has_many :comments, class_name: 'Discussion', :foreign_key => "discussion_id", :dependent => :destroy
  belongs_to :discussion, :counter_cache => :comments_count, inverse_of: :comments
  validates :message, presence: true

  scope :recents, ->{ order(created_at: :desc) }
end
