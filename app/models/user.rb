class User < ActiveRecord::Base
  has_one :profile, :dependent => :destroy
  has_many :articles, :dependent => :destroy
  has_many :discussions, :dependent => :destroy
  after_create :my_profile
  validates :email, presence: false, uniqueness: true, format: {with:Devise.email_regexp, allow_blank: true}
  mount_uploader :avatar, AvatarUploader
  validates_integrity_of  :avatar
  validates_processing_of :avatar

  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :confirmable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  private
  def my_profile
    profile = Profile.create(:user_id => self.id)
    profile.save
  end
end
