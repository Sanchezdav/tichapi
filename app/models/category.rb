class Category < ApplicationRecord
  has_many :subcategories, dependent: :destroy
  has_many :articles, through: :subcategories
  validates :name, presence: true, uniqueness: true
  validates :slug, uniqueness: true

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]
end
