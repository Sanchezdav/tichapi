class Profile < ApplicationRecord
  belongs_to :user
  before_create :create_alias
  validates :alias, uniqueness: { message: "ya ha sido tomado, modifíquelo por favor!" }, allow_blank: true
  
  def create_alias
    self.alias = SecureRandom.hex(10)
  end
end
