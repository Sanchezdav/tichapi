Rails.application.routes.draw do
  apipie
  mount_devise_token_auth_for 'User', at: 'auth', :controllers => { omniauth_callbacks: 'users/omniauth_callbacks'}

  scope "admin", module: "admin", defaults:{format: 'json'} do
    resources :categories, only: :index
    resources :subcategories, only: :index
    resources :articles
  end

  scope "me", module: "me", defaults:{format: 'json'} do
    get '', to: 'profiles#show'
    put '/:id', to: 'profiles#update'
  end

  resources :categories, only: [:index], defaults:{format: 'json'}
  resources :subcategories, only: [:index], defaults:{format: 'json'}
  resources :articles, only: [:index, :show, :update], defaults:{format: 'json'} do
    resources :discussions, only: [:index, :create, :update], defaults:{format: 'json'} do
      resources :comments, only: [:create, :update], defaults:{format: 'json'}
    end
  end
end