Apipie.configure do |config|
  config.app_name                = "Tichapi"
  config.api_base_url            = "/"
  config.doc_base_url            = "/api_doc"
  config.validate = false
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/{[!concerns/]**/*,*}.rb"
end
