source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.0.rc1'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
# Use Puma as the app server
gem 'puma'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.0'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

# My gems
# Custom json response for each model
gem 'active_model_serializers', github: "rails-api/active_model_serializers"
# Secure configure Rails applications
gem 'figaro'
# Mysql as database engine
gem 'mysql2'
# Authentication by token
gem 'devise_token_auth', github:"lynndylanhurley/devise_token_auth", branch: "master"
gem "devise", :github => 'plataformatec/devise', :branch => 'master'
# Social authentication
gem "omniauth"
gem 'omniauth-facebook'
gem 'omniauth-twitter'
gem "omniauth-google-oauth2"
# Provides a simple and extremely flexible way to upload files from Ruby application
gem 'carrierwave'
# Optimizer image
gem 'carrierwave-imageoptimizer'
# Process image
gem "mini_magick"
# Friendly url by slug
gem 'friendly_id', '~> 5.1.0'
# applies a patch to OpenURI to optionally allow redirections from HTTP to HTTPS, or from HTTPS to HTTP
gem 'open_uri_redirections'
# Dont destroy elements
gem "paranoia", :github => "radar/paranoia", :branch => "rails4"
# Documentation of apis
gem 'apipie-rails'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
end

group :development do
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  #Inbox in a tab
  gem "letter_opener"
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
