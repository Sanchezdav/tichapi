## README

# Tich #

Sitio web que comparte artículos sobre informática y tecnología.

## Tecnologías

Creado con Ruby 2.3 y el framework Ruby on Rails en su versión 5 por medio de Apis y con una base de datos manejada por mysql con la gema mysql2

## Instalación

Para poder correr el proyecto es necesario instalar Ruby 2.2 o mayor y Rails 5, así como mysql server para el manejo de la base de datos.

## Colaboradores

### Fausto David Sánchez Salazar
### Ing. en Informática
Twitter: @SanchezDav90